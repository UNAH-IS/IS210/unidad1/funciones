﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funciones
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Uso de argumentos por defecto");
            Saludar("a todos");
            Saludar();
            /////////////////////////////////////////////////////////////////////

            int x0 = 100;
            Console.WriteLine($"\nValor original: {x0}");
            PasarPorValor(x0);
            Console.WriteLine($"Valor nuevo (pasado por valor): {x0}");

            int x1 = 100;
            Console.WriteLine($"\nValor original: {x1}");
            PasarPorReferencia(ref x1);
            Console.WriteLine($"Valor nuevo (pasado por referencia): {x1}");

            /////////////////////////////////////////////////////////////////////

            Console.WriteLine("\nUso de argumentos de forma dinamica");
            Console.WriteLine($"1 + 2 + 3 = { Sumar(1, 2, 3) }");
            Console.WriteLine($"1 + 2 + 3 + 4 = { Sumar(1, 2, 3, 4) }");

            /////////////////////////////////////////////////////////////////////

            Console.WriteLine("\nPara el arreglo siguiente:");
            int[] arreglo = new int[] { 10, 20, 30, 40 };

            foreach (int item in arreglo)
            {
                Console.Write($"{item,4}");
            }

            int indice = 1;
            int valor = 100;
            cambiarElemento(arreglo, indice, valor);
            Console.WriteLine("\nElementos despues de modificacion");

            foreach (int item in arreglo)
            {
                Console.Write($"{item,4}");
            }


            /////////////////////////////////////////////////////////////////////

            Console.WriteLine($"\n\nTamaño original es: {arreglo.Length}");
            Array.Resize(ref arreglo, 20);
            Console.WriteLine($"Nuevo tamaño es: {arreglo.Length}");

                       
            int[] a1 = new int[] { 1, 2, 3, 4 };
            int[] cuadrados = potenciarArreglo(a1, 2);

            foreach(int base1 in cuadrados)
            {
                Console.Write($"{base1, 4}");
            }

            Console.ReadKey();
        }

        static void Saludar(string mensaje = "Mundo")
        {
            Console.WriteLine($"Hola {mensaje}");
        }

        static void PasarPorValor(int a)
        {
            a = 99;
        }

        static void PasarPorReferencia(ref int a)
        {
            a = 99;
        }
        
        // Funcion que permite utilizar varios argumentos de forma dinamica
        static int Sumar(params int[] args)
        {
            int total = 0;

            foreach(int elemento in args)
            {
                total += elemento;
            }

            return total;
        }

        static void cambiarElemento(int[] arreglo, int indice, int nuevoValor)
        {
            if (indice < arreglo.Length && indice > 0)
            {
                arreglo[indice] = nuevoValor;
            }
        }

        static int[] potenciarArreglo(int[] bases, int potencia)
        {
            // Crear un arreglo de copia
            int[] copia = new int[bases.Length];

            // Iterar cada elemento obteniendo la potencia
            for (int i = 0; i < bases.Length; i++)
            {
                copia[i] = (int)Math.Pow(bases[i], potencia);
            }

            // Retornar el valor modificado
            return copia;
        }

    }
}
